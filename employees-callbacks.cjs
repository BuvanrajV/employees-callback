/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/

const fs = require("fs");

function employeesCallBack() {
  fs.readFile("data.json", "utf-8", (err, data) => {
    if (err) {
      console.error(err);
    } else {
      let objData = JSON.parse(data);
      let key = Object.keys(objData);
      let value = Object.values(objData);
      value = value[0];
      let rest = [2, 13, 23];
      // Problem 1 retrive id
      let retriveId = value.filter((item) => {
        if (rest.includes(item.id)) {
          return item;
        }
      });
      fs.writeFile("problem1.json", JSON.stringify(retriveId), (err) => {
        if (err) {
          console.log(err);
        } else {
          // Problem 2
          let groupData = value.reduce((accu, curr) => {
            let company = curr.company;
            if (accu.hasOwnProperty(company)) {
              accu[company].push(curr);
            } else {
              accu[company] = [curr];
            }
            return accu;
          }, {});
          fs.writeFile("problem2.json", JSON.stringify(groupData), (err) => {
            if (err) {
              console.error(err);
            } else {
              // Problem 3
              let powerPuffBrigade = value.filter(
                (item) => item.company === "Powerpuff Brigade"
              );
              fs.writeFile(
                "problem3.json",
                JSON.stringify(powerPuffBrigade),
                (err) => {
                  if (err) {
                    console.log(err);
                  } else {
                    // Problem 4
                    let removeEntry = value.filter((item) => item.id !== 2);
                    fs.writeFile(
                      "problem4.json",
                      JSON.stringify(removeEntry),
                      (err) => {
                        if (err) {
                          console.log(err);
                        } else {
                          //Problem 5
                          let sortComapanies = value.sort(
                            (employee1, employee2) => {
                              if (employee1.company === employee2.company) {
                                if (employee1.id < employee2.id) {
                                  return -1;
                                } else {
                                  return 1;
                                }
                              } else {
                                if (employee1.company < employee2.company) {
                                  return -1;
                                } else {
                                  return 1;
                                }
                              }
                            }
                          );
                          fs.writeFile(
                            "problem5.json",
                            JSON.stringify(sortComapanies),
                            (err) => {
                              if (err) {
                                console.error(err);
                              } else {
                                //Problem 7
                                let addDob = value.map((item) => {
                                  let id = item.id;
                                  if (id % 2 == 0) {
                                    let date = new Date();
                                    item["DOB"] = date;
                                  }
                                  return item;
                                });
                                fs.writeFile(
                                  "problem7.json",
                                  JSON.stringify(addDob),
                                  (err) => {
                                    if (err) {
                                      console.error(err);
                                    }
                                  }
                                );
                              }
                            }
                          );
                        }
                      }
                    );
                  }
                }
              );
            }
          });
        }
      });
    }
  });
}

employeesCallBack();
